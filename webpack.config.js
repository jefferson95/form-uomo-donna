const path = require('path');
module.exports = {
  devtool: 'eval-source-map',
  entry: './src/index.js',
  mode: 'development',
  module: {
    rules: [{
      exclude: /node_modules/,
      use: [{
        loader:'babel-loader',
      }],
      test: /\.jsx?$/
    }],  
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  }
}
  