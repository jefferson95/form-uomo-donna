export const colpa = u => 90* u / 100;

export function validator(e, calcolo) {
    if (e.target.value >= 0 && e.target.value <= 100){
        calcolo.textContent = `La tua colpa è del` + ` ` + colpa(e.target.value) + ` ` + `%`;
        return;
    }
 //scrivendo "return" dopo l'ipotetico caso "true" ti permette di omettere else.
    calcolo.textContent = "";
    e.target.value = "";
}